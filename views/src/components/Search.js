import { Card, Col, Container, Row } from "react-bootstrap";
import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { Slide, toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";

import DashboardSpinner from "./dashboard/DashboardSpinner";
import { Link } from "react-router-dom";
import MainPagination from "./MainPagination";
import { addToCart } from "../redux/actions/cart-actions/addToCart";
import { addToWishlist } from "./../redux/actions/wishlist-actions/addToWishlist";
import { fetchProducts } from "../redux/actions/product-actions/fetchProductsAction";

const Search = () => {

  const [value, setValue] = useState('');
  const onChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <div className='col-lg-6  col-sm-4 navbar-search'>
        <div className='input-group'>
          <input type='text' value={value} onChange={onChange} className='form-control' placeholder='Search ......' />
          <div className='input-group-append'>
            <Link to={`/products?name=`+value}>
              <span className='input-group-text'>
                <i className='fa fa-search'></i>
              </span>
            </Link>
          </div>
        </div>
    </div>
  );
};

export default Search;
